package net.kohding.lvltrack;

import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by Juan on 1/7/2015.
 */
public class Player {
    TextView level;
    ImageButton up, down;
    ProgressBar pb;
    TextView name;

    Player(TextView name, TextView level, ImageButton up, ImageButton down, ProgressBar pb){
        this.level = level;
        this.up = up;
        this.down = down;
        this.pb = pb;
        this.name = name;

        up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                levelUp();
                int progress = getlvl()*10;
                Log.d("lvltrk", String.valueOf(progress));
                setProgBar(progress);
            }
        });
        down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                levelDown();
                int progress = getlvl()*10;
                Log.d("lvltrk", String.valueOf(progress));
                setProgBar(progress);
            }
        });
    }

    private void levelUp(){
        int current = getlvl();
        if(current < 10){
            level.setText(String.valueOf(++current));
        }
    }

    private void levelDown(){
        int current = getlvl();
        if(current > 1){
            level.setText(String.valueOf(--current));
        }
    }

    int getlvl(){
        return Integer.valueOf(level.getText().toString());
    }

    private void setProgBar(int progress){
        pb.setProgress(progress);
    }

    void resetScore(){
        level.setText(String.valueOf(1));
        setProgBar(10);
    }
}
