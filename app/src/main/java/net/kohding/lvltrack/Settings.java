package net.kohding.lvltrack;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;


public class Settings extends Activity {

    EditText[] pset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        pset = new EditText[6];

        pset[0] = (EditText)findViewById(R.id.setP1);
        pset[1] = (EditText)findViewById(R.id.setP2);
        pset[2] = (EditText)findViewById(R.id.setP3);
        pset[3] = (EditText)findViewById(R.id.setP4);
        pset[4] = (EditText)findViewById(R.id.setP5);
        pset[5] = (EditText)findViewById(R.id.setP6);

        for(int i = 0; i < pset.length; i++){
            pset[i].setText(MainActivity.players[i].name.getText());
        }
    }

    @Override
    protected void onStop(){
        super.onStop();

        for(int i = 0; i < pset.length; i++){
            MainActivity.players[i].name.setText(pset[i].getText());
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
