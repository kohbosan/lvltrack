package net.kohding.lvltrack;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


public class MainActivity extends Activity {

    LinearLayout tl;
    ProgressBar[] pbs;

    public static Player[] players;
    int maxlevel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        maxlevel = 10;

        players = new Player[6];
        players[0] = new Player(
                (TextView)findViewById(R.id.p1name),
                (TextView)findViewById(R.id.p1lvl),
                (ImageButton) findViewById(R.id.p1up),
                (ImageButton) findViewById(R.id.p1down),
                (ProgressBar) findViewById(R.id.progressBar1));
        players[1] = new Player(
                (TextView)findViewById(R.id.p2name),
                (TextView)findViewById(R.id.p2lvl),
                (ImageButton) findViewById(R.id.p2up),
                (ImageButton) findViewById(R.id.p2down),
                (ProgressBar) findViewById(R.id.progressBar2));
        players[2] = new Player(
                (TextView)findViewById(R.id.p3name),
                (TextView)findViewById(R.id.p3lvl),
                (ImageButton) findViewById(R.id.p3up),
                (ImageButton) findViewById(R.id.p3down),
                (ProgressBar) findViewById(R.id.progressBar3));
        players[3] = new Player(
                (TextView)findViewById(R.id.p4name),
                (TextView)findViewById(R.id.p4lvl),
                (ImageButton) findViewById(R.id.p4up),
                (ImageButton) findViewById(R.id.p4down),
                (ProgressBar) findViewById(R.id.progressBar4));
        players[4] = new Player(
                (TextView)findViewById(R.id.p5name),
                (TextView)findViewById(R.id.p5lvl),
                (ImageButton) findViewById(R.id.p5up),
                (ImageButton) findViewById(R.id.p5down),
                (ProgressBar) findViewById(R.id.progressBar5));
        players[5] = new Player(
                (TextView)findViewById(R.id.p6name),
                (TextView)findViewById(R.id.p6lvl),
                (ImageButton) findViewById(R.id.p6up),
                (ImageButton) findViewById(R.id.p6down),
                (ProgressBar) findViewById(R.id.progressBar6));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.clearscores) {
            clearScores();
            return true;
        }
        if (id == R.id.settings) {
            Intent intent = new Intent(this, Settings.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setProgBarValue(ProgressBar pb, float weight){
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT,
                weight);
        pb.setLayoutParams(params);
    }

    private void clearScores(){
        for(Player p : players){
            p.resetScore();
        }
    }

    public void setPlayerName(int index, String name){
        players[index].name.setText(name);
    }

}
